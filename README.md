# Soapbox Divi Child Theme

This repo contains the WordPress theme used by https://soapbox.pub

It is a child theme of [Divi](https://www.elegantthemes.com/blog/divi-resources/divi-child-theme).

## Paid plugins

soapbox.pub runs entirely Free Software, but some plugins are paid for.
For developer convenience, they can be downloaded from here:

- Divi theme ([Divi.zip](https://media.soapbox.pub/uploads/2022/02/Divi.zip))
- GiveWP recurring donations ([give-recurring.zip](https://media.soapbox.pub/uploads/2022/02/give-recurring.zip))

We ask that you please support Free Software development by paying for these plugins if you intend to run them outside the context of Soapbox website contributions.
