<?php

function soapbox_enqueue_styles() {
    # Divi parent theme CSS
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

function soapbox_print_styles() {
    # GiveWP overrides
    wp_enqueue_style(
        'givewp-iframes-styles',
        get_stylesheet_directory_uri() . '/givewp-iframes-styles.css',
        ['give-sequoia-template-css']
    );
}

add_action( 'wp_enqueue_scripts', 'soapbox_enqueue_styles' );
add_action( 'wp_print_styles', 'soapbox_print_styles', 10);
